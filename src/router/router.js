import Vue from 'vue'
import VueRouter from 'vue-router'

import GridGrouping from '../components/GridGrouping.vue'
import GridSelect from '../components/GridSelect.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode    : 'history',
    routes  : [
        {
            path        : '/',
            name        : 'login',
            component   : () => import("@/views/Login.vue")
        },
        {
            path        : '/home',
            name        : 'home',
            component   : () => import("@/views/rekam_medis/Home.vue")
        },
        {
            path        : '/registrasi',
            name        : 'registrasi',
            component   : () => import("@/views/rekam_medis/Registrasi.vue")
        },
        {
            path        : '/daftar-pengunjung',
            name        : 'DaftarPengunjung',
            component   : () => import("@/views/rekam_medis/Daftar_Pengunjung.vue")
        },
        {
            path        : '/daftar-kunjungan',
            name        : 'DaftarKunjungan',
            component   : () => import("@/views/rekam_medis/Daftar_Kunjungan.vue")
        },
        {
            path        : '/detail-pasien',
            name        : 'DetailPasien',
            component   : () => import("@/views/rekam_medis/Detail_Pasien.vue")
        },
        {
            path        : '/sidebar',
            name        : 'sidebar',
            component   : () => import("@/components/Sidebar2.vue")
        },
        {
            path        : '/gridselect',
            name        : 'gridselect',
            component   : GridSelect
        },
        {
            path        : '/gridgrouping',
            name        : 'gridgrouping',
            component   : GridGrouping
        },
        {
            path        : '/gridgroupselect',
            name        : 'gridgroupselect',
            component   : () => import("@/components/GridGroupSelect.vue")
        },
        {
            path        : '/display-pemanggil',
            name        : 'DisplayPemanggil',
            component   : () => import("@/views/pemanggil/DisplayPemanggil.vue")
        },
        {
            path        : '/home-perawat',
            name        : 'HomePerawat',
            component   : () => import("@/views/perawat/Home_Perawat.vue")
        },
        {
            path        : '/daftar-antrian-pasien',
            name        : 'DaftarAntrian',
            component   : () => import("@/views/perawat/Daftar_Antrian.vue")
        },
        {
            path        : '/rm/home',
            name        : 'Home',
            component   : () => import("@/views/rekam_medis/Home.vue")
        },
        {
            path        : '/it/home',
            name        : 'Home',
            component   : () => import("@/views/it/Home.vue")
        },
        {
            path        : '/it/setup/menu',
            name        : 'SetupMenu',
            component   : () => import("@/views/it/setup/Setup_Menu.vue")
        }
    ]
})

export default router