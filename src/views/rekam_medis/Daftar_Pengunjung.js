 //import component
 import Header from "@/components/Header"
 import Sidebar from "@/components/Sidebar"
 import GridSelect from "@/components/GridSelect"
import axios from "axios"

 export default {
     name: 'DaftarPengunjung',
     components: {
         Header,
         Sidebar,
         GridSelect
     },
     data(){
         return{
            ruangan_selected: 1,
            ruangan_options:[
                { value: '1', text: 'Sultan' },
                { value: '2', text: 'Kristen' },
            ],
         }
     },
     mounted(){
        //  axios.get("http://localhost:8080/get-pasien")
        //  .then(response=>(this.info = response))
        axios
      .get('http://localhost:8080/get-pasien')
      .then(response => {this.info = response.data[0]['namapasien']
        console.log(response.data[0]['namapasien'])})
     }
 }