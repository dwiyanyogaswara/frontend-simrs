 //import component
 import Header from "@/components/Header"
 import Sidebar from "@/components/Sidebar"

 export default {
     name: 'registrasi',
     components: {
         Header,
         Sidebar
     },
      data() {
         return {
             nik: '',
             nama: '',
             agama_selected: 1,
             agama_options:[
                 { value: '1', text: 'Islam' },
                 { value: '2', text: 'Kristen' },
             ],
             rujukan_selected: 1,
             rujukan_options:[
                 { value: '1', text: 'Ya' },
                 { value: '0', text: 'Tidak' },
             ],
             kasus_selected: 1,
             kasus_options:[
                 { value: '1', text: 'Ya' },
                 { value: '0', text: 'Tidak' },
             ]
         }
     }
 }