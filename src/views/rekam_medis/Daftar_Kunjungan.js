 //import component
 import Header from "@/components/Header"
 import Sidebar from "@/components/Sidebar"
 import GridSelect from "@/components/GridSelect"

 export default {
     name: 'DaftarKunjungan',
     components: {
         Header,
         Sidebar,
         GridSelect
     },
     data(){
         return{
            ruangan_selected: 1,
            ruangan_options:[
                { value: '1', text: 'Sultan' },
                { value: '2', text: 'Kristen' },
            ],
         }
     }
 }