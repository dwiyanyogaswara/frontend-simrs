import axios from "axios"
import VueCookies from "vue-cookies"

export default {
    name: 'Login',
    data(){
        return{
           email: '',
           password: ''
        }
    },
    methods: {
        login(){
            var body = {
                "email" : this.email,
                "password" : this.password
            }
            axios
                .post('http://localhost:8080/api/auth/login',
                    body,
                    {
                        headers: {
                            'Content-type': 'application/json'
                        }
                    }
                )
                .then(response => {
                    // this.info = response.data
                    VueCookies.set("token", response.data.token)
                    VueCookies.set("id_user", response.data.data.id)
                    VueCookies.set("name", response.data.data.name)
                    VueCookies.set("email", response.data.data.email)
                    VueCookies.set("id_role", response.data.data.id_role)
                    VueCookies.set("role", response.data.data.role)
                    // console.log(VueCookies.get("token"))
                    this.redirectMenu()
                })
        },
        redirectMenu(){
            if(VueCookies.get("role")=="it"){ 
                this.$router.push('/it/home')
            }
        }
    }
}