//import component
import Header from "@/components/Header"
import Sidebar from "@/components/Sidebar"
import GridSelect from "@/components/GridSelect"
import axios from "axios"
import VueCookies from "vue-cookies";

export default {
    name: 'SetupMenu',
    components: {
        Header,
        Sidebar,
        GridSelect
    },
    data() {
        return {
            ruangan_selected: 1,
            ruangan_options: [
                { value: '1', text: 'Sultan' },
                { value: '2', text: 'Kristen' },
            ],
        }
    },
    methods: {
        getMenuAll() {
            axios
                .get("http://localhost:8080/api/it/menu/get-menu-all",
                    {
                        'headers': {
                            'Authorization': 'Bearer ' + VueCookies.get("token")
                        }
                    }
                )
                .then((response) => {
                    console.log(response.data);
                    this.menu = response.data
                });
        },
    },
    created() {
        this.getMenuAll();
    }
}