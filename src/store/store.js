import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)


export default new Vuex.Store({
  state : {
    count: 0
  },
  mutations: {
    increment (state, val){
      state.count= val
    }
  },
  actions:{
    setCount({commit, state}, newval){
      commit("increment", newval)
      return state.count
    }
  }
})