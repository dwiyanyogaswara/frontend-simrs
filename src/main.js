import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import { BootstrapVue } from 'bootstrap-vue'
import VueSidebarMenu from 'vue-sidebar-menu'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import stockInit from 'highcharts/modules/stock'
import mapInit from 'highcharts/modules/map'
import addWorldMap from './worldmap'
import store from './store/store'


// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'

Vue.config.productionTip = false

stockInit(Highcharts)
mapInit(Highcharts)
addWorldMap(Highcharts)

Vue.use(BootstrapVue)
Vue.use(VueSidebarMenu)
Vue.use(HighchartsVue)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
